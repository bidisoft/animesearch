package com.example.seb.animesearch;

import org.json.JSONObject;

import java.io.Serializable;


public class Anime implements Serializable {

    /**
     * Constructeur privé
     */
    private Anime() {
        slug = "";
        url = "";
        title = "";
        episode_count = "";
        cover_image = "";
        synopsis = "";
    }

    /**
     * Instance unique pré-initialisée
     */
    private static Anime INSTANCE = new Anime();

    /**
     * Point d'accès pour l'instance unique du singleton
     */
    public static Anime getInstance() {
        return INSTANCE;
    }

    public static String S_RESULT_JSON_KEY_SLUG = "slug";
    public static String S_RESULT_JSON_KEY_URL = "url";
    public static String S_RESULT_JSON_KEY_TITLE = "title";
    public static String S_RESULT_JSON_KEY_EPISODE_COUNT = "episode_count";
    public static String S_RESULT_JSON_KEY_COVER_IMAGE = "cover_image";
    public static String S_RESULT_JSON_KEY_SYNOPSIS = "synopsis";
    public static String S_RESULT_JSON_KEY_ERROR = "error";

    private String slug;
    private String url;
    private String title;
    private String episode_count;
    private String cover_image;
    private String synopsis;
    private String error;

    public static void reinit() {
        getInstance().slug = "";
        getInstance().url = "";
        getInstance().title = "";
        getInstance().episode_count = "";
        getInstance().cover_image = "";
        getInstance().synopsis = "";
        getInstance().error = "";
    }


    public static void initAnime(JSONObject json) {
        getInstance().slug = json.optString(S_RESULT_JSON_KEY_SLUG);
        getInstance().url = json.optString(S_RESULT_JSON_KEY_URL);
        getInstance().title = json.optString(S_RESULT_JSON_KEY_TITLE);
        getInstance().episode_count = json.optString(S_RESULT_JSON_KEY_EPISODE_COUNT);
        getInstance().cover_image = json.optString(S_RESULT_JSON_KEY_COVER_IMAGE);
        getInstance().synopsis = json.optString(S_RESULT_JSON_KEY_SYNOPSIS);
        getInstance().error = json.optString(S_RESULT_JSON_KEY_ERROR);
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEpisode_count() {
        return episode_count;
    }

    public void setEpisode_count(String episode_count) {
        this.episode_count = episode_count;
    }

    public String getCover_image() {
        return cover_image;
    }

    public void setCover_image(String cover_image) {
        this.cover_image = cover_image;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
