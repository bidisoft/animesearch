package com.example.seb.animesearch;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView title;
    TextView error;
    EditText searchEditText;
    ProgressBar bar;
    ImageButton searchButton;
    AlertDialog.Builder builder;

    public String getHtml(String color, char character) {
        return "<font color='".concat(color).concat("'>").concat(String.valueOf(character)).concat("</font>");
    }

    private void init() {
        // Get objects
        searchButton = (ImageButton) findViewById(R.id.searchImageButton);
        searchEditText = (EditText) findViewById(R.id.animeEditText);
        searchEditText.setImeActionLabel("Search", KeyEvent.KEYCODE_ENTER);
        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                searchAnime();
                return true;
            }
        });
        bar = (ProgressBar) findViewById(R.id.progressBar);
        title = (TextView) findViewById(R.id.title);

        // (Re)initialise value
        searchEditText.setText("");
    }

    private void initActions() {
        searchButton.setVisibility(View.VISIBLE);
        bar.setVisibility(View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        // Initialise activity
        init();

        initActions();

        // (Re)-initialise Anime instance
        Anime.getInstance().reinit();

        // Fancy title orange and white
        String text = "";
        for (char i = 0; i < title.getText().toString().length(); i++) {
            text = text.concat(getHtml((i % 2 == 0 ? "orange" : "white"), title.getText().toString().charAt(i)));
        }
        title.setText(Html.fromHtml(text));

        // Alert dialog in case of problems...
        builder = new AlertDialog.Builder(this);
        builder.setTitle("Error")
                .setPositiveButton("OK", null);
        builder.create();

        // Listener for click on action image button
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchAnime();
            }
        });
    }

    private void searchAnime() {
        // If not empty...
        if (!searchEditText.getText().toString().isEmpty()) {
            // Create async task
            SearchAnimeTask task = new SearchAnimeTask();
            // delegate output logic
            task.setmDelegate(new SearchAnimeListener() {
                @Override
                public void processFinish() {
                    // Display dialog in case of problems
                    if (Anime.getInstance().getError() != null && !Anime.getInstance().getError().isEmpty()) {
                        builder.setMessage(Anime.getInstance().getError()).setPositiveButton(R.string.okButton, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                initActions();
                            }
                        });
                        builder.show();
                    } else {
                        // jump to next activity
                        Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                        startActivity(intent);
                    }
                }
            });
            // switch image to progress bar
            searchButton.setVisibility(View.GONE);
            bar.setVisibility(View.VISIBLE);

            // execute task
            task.execute(searchEditText.getText().toString());
        } else {
            // If title edit box is empty then display dialog message
            builder.setMessage(R.string.emptyTitleMsg);
            builder.show();
        }
    }
}
