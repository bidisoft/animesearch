package com.example.seb.animesearch;

import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import org.json.JSONObject;

/**
 * Created by sebs on 17/09/15.
 */
public class SearchAnimeTask extends AsyncTask<String, Void, Void> {

    private SearchAnimeListener mDelegate;

    public SearchAnimeListener getmDelegate() {
        return mDelegate;
    }

    public void setmDelegate(SearchAnimeListener mDelegate) {
        this.mDelegate = mDelegate;
    }

    SearchAnimeTask() {

    }

    @Override
    protected Void doInBackground(String... params) {
        String title = params[0].replaceAll(" ", "-").toLowerCase();
        RestClient rc = new RestClient("https://hummingbirdv1.p.mashape.com/anime/".concat(title));
        rc.addHeader("X-Mashape-Key", "t9UvtPKJiKmshv7eUo5mzdAFIxbOp108kVRjsnvKF037Z2XRme");
        rc.addHeader("Accept", "application/json");

        try {
            rc.execute(RestClient.RequestMethod.GET);
            System.out.println(rc.getResponse());
            Anime.getInstance().initAnime(new JSONObject(rc.getResponse()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void lc) {
        mDelegate.processFinish();
    }

}