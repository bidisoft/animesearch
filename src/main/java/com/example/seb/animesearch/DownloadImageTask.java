package com.example.seb.animesearch;

import android.os.AsyncTask;
import android.widget.TextView;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * Created by sebs on 17/09/15.
 */
public class DownloadImageTask extends AsyncTask<String, Void, String> {

    TextView textView;
    OkHttpClient client = new OkHttpClient();

    DownloadImageTask(TextView tv) {
        textView = tv;
    }

    public String run(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();


        Response response = client.newCall(request).execute();
        String result = response.body().string();
        return result;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            return run(params[0].toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        textView.setText(s);
    }
}
