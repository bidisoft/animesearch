package com.example.seb.animesearch;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    TextView title;
    TextView episodeCount;
    TextView synopsis;
    ImageView image;

    private void init() {
        title = (TextView) findViewById(R.id.resultTitleTextView);
        episodeCount = (TextView) findViewById(R.id.resultEpisodeCountTextView);
        synopsis = (TextView) findViewById(R.id.resultSynopsisTextView);
        image = (ImageView) findViewById(R.id.resultImageView);

        title.setText("");
        episodeCount.setText("");
        synopsis.setText("");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_result);

        // Initialise activity
        init();

        title.setText(Anime.getInstance().getTitle());
        episodeCount.setText(Anime.getInstance().getEpisode_count());
        synopsis.setText(Anime.getInstance().getSynopsis());

        ImageDownloader id = new ImageDownloader();
        id.download(Anime.getInstance().getCover_image().replace("https", "http"), image);

        Button restart = (Button) findViewById(R.id.backButton);
        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

}
